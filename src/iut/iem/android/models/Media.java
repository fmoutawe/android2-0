package iut.iem.android.models;

import android.widget.ProgressBar;

public class Media {

	private int id;
	private Integer version, newVersion;
	private String type, name, path;
	private boolean installed;
	private ProgressBar progressBar;
	private int progress;
	private boolean installing, updatable;

	public Media(String type, String name, Integer version, String path) {
		this(type, name, version, path, 0, false);
	}

	public Media(String type, String name, Integer version, String path,
			int id) {
		this(type, name, version, path, id, false);
	}

	public Media(String type, String name, Integer version, String path, int id,
			boolean installed) {
		setId(id);
		setType(type);
		setName(name);
		setVersion(version);
		setPath(path);
		setInstalled(installed);
		setInstalling(false);
		setNewVersion(0);
		setUpdatable(false);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isInstalled() {
		return installed;
	}

	public void setInstalled(boolean installed) {
		this.installed = installed;
	}
	
	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public void setProgressBar(ProgressBar progress) {
		this.progressBar = progress;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}
	
	public boolean isInstalling(){
		return installing;
	}
	
	public void setInstalling(boolean installing){
		this.installing = installing;
	}
	
	public Integer getNewVersion() {
		return newVersion;
	}

	public void setNewVersion(Integer newVersion) {
		this.newVersion = newVersion;
	}

	public boolean isUpdatable() {
		return updatable;
	}

	public void setUpdatable(boolean updatable) {
		this.updatable = updatable;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.type.hashCode() * 72 + this.name.hashCode() * 63 + this.path.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Media) {
			Media media = (Media) o;
			return getType().equals(((Media) o).getType())
					&& getName().equals(media.getName())
					&& getPath().equals(media.getPath());
		}
		return false;
	}
	
	public boolean equalVersion(Media media){
		return getVersion().equals(media.getVersion());
	}
	
	public boolean lowerVersion(Media media){
		return getVersion() < media.getVersion();
	}
	
}
