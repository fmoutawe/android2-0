package iut.iem.android.receivers;

import iut.iem.android.others.Utilities;
import iut.iem.android.tasks.MediaParserTask;

import java.io.File;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MediaFileDownloadEndedSuccessReceiver extends BroadcastReceiver {
	public final static String LOG_TAG = MediaFileDownloadEndedSuccessReceiver.class.getSimpleName();
	
	public MediaFileDownloadEndedSuccessReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String mediaFilePath = context.getFilesDir().getAbsolutePath()
				+ File.separator + Utilities.MEDIA_FILE_NAME;
		MediaParserTask mediaParser = new MediaParserTask(context);
		mediaParser.execute(mediaFilePath);
	}
}
