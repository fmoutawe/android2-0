package iut.iem.android.receivers;

import iut.iem.android.others.Utilities;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MediaFileDownloadEndedFailureReceiver extends BroadcastReceiver {
	
	public static final String LOG_TAG = MediaFileDownloadEndedFailureReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
			Utilities.notificationFailureUpdateMedia(context);
	}
}
