package iut.iem.android.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteDatabase extends SQLiteOpenHelper {

	public static final String MEDIA_TABLE = "media";

	public static final String COL_ID = "ID";
	public static final String COL_TYPE = "TYPE";
	public static final String COL_NAME = "NAME";
	public static final String COL_PATH = "PATH";
	public static final String COL_VERSION = "VERSION";
	public static final String COL_NEW_VERSION = "NEW_VERSION";
	public static final String COL_INSTALLED = "INSTALLED";
	public static final String COL_UPDATABLE = "UPDATABLE";

	public static final int NUM_COL_ID = 0;
	public static final int NUM_COL_TYPE = 1;
	public static final int NUM_COL_NAME = 2;
	public static final int NUM_COL_PATH = 3;
	public static final int NUM_COL_VERSION = 4;
	public static final int NUM_COL_NEW_VERSION = 5;
	public static final int NUM_COL_INSTALLED = 6;
	public static final int NUM_COL_UPDATABLE = 7;

	public static final String CREATE_TABLE = "CREATE TABLE " + MEDIA_TABLE
			+ " ( " + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_TYPE
			+ " TEXT NOT NULL, " + COL_NAME + " TEXT NOT NULL, " + COL_PATH
			+ " TEXT NOT NULL, " + COL_VERSION + " NUMBER NOT NULL, "
			+ COL_NEW_VERSION + " INTEGER, " + COL_INSTALLED + " BOOLEAN, "
			+ COL_UPDATABLE + " BOOLEAN);";
	
	public static final String DROP_TABLE = "DROP TABLE" + MEDIA_TABLE + ";";

	public MySQLiteDatabase(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DROP_TABLE);
		onCreate(db);
	}

}
