package iut.iem.android.database;

import iut.iem.android.models.Media;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MediaBdd {

	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "media.bdd";

	private SQLiteDatabase database;
	private MySQLiteDatabase myDatabase;

	public static class Builder {

		private static MediaBdd instance;

		public static MediaBdd getInstance(Context context) {
			if (instance == null) {
				instance = new MediaBdd(context);
			}
			return instance;
		}
	}

	public MediaBdd(Context context) {
		myDatabase = new MySQLiteDatabase(context, NOM_BDD, null, VERSION_BDD);
	}

	public void open() {
		database = myDatabase.getWritableDatabase();
	}

	public void close() {
		database.close();
	}

	public long insertMedia(Media media) {
		ContentValues values = new ContentValues();

		values.put(MySQLiteDatabase.COL_TYPE, media.getType());
		values.put(MySQLiteDatabase.COL_NAME, media.getName());
		values.put(MySQLiteDatabase.COL_PATH, media.getPath());
		values.put(MySQLiteDatabase.COL_VERSION, media.getVersion());
		values.put(MySQLiteDatabase.COL_NEW_VERSION, media.getNewVersion());
		values.put(MySQLiteDatabase.COL_INSTALLED, media.isInstalled());
		values.put(MySQLiteDatabase.COL_UPDATABLE, media.isUpdatable());

		return database.insert(MySQLiteDatabase.MEDIA_TABLE, null, values);
	}

	public int updateMedia(Media media) {
		ContentValues values = new ContentValues();

		values.put(MySQLiteDatabase.COL_TYPE, media.getType());
		values.put(MySQLiteDatabase.COL_NAME, media.getName());
		values.put(MySQLiteDatabase.COL_PATH, media.getPath());
		values.put(MySQLiteDatabase.COL_VERSION, media.getVersion());
		values.put(MySQLiteDatabase.COL_NEW_VERSION, media.getNewVersion());
		values.put(MySQLiteDatabase.COL_INSTALLED, media.isInstalled());
		values.put(MySQLiteDatabase.COL_UPDATABLE, media.isUpdatable());

		return database.update(MySQLiteDatabase.MEDIA_TABLE, values,
				MySQLiteDatabase.COL_ID + " = '" + media.getId() + "'", null);
	}

	public int removeMedia(Media media) {
		return database.delete(MySQLiteDatabase.MEDIA_TABLE,
				MySQLiteDatabase.COL_ID + " = " + media.getId() + "'", null);
	}

	public List<Media> getMediasByType(String type) {
		List<Media> medias = new ArrayList<Media>();
		open();
		Cursor c = database.rawQuery("select * from "
				+ MySQLiteDatabase.MEDIA_TABLE + " where "
				+ MySQLiteDatabase.COL_TYPE + " = '" + type + "'", null);
		while (c.moveToNext()) {
			medias.add(cursorToMedia(c));
		}
		c.close();
		close();
		return medias;
	}
	
	public List<Media> getMedias() {
		List<Media> medias = new ArrayList<Media>();
		open();
		Cursor c = database.rawQuery("select * from "
				+ MySQLiteDatabase.MEDIA_TABLE, null);
		while (c.moveToNext()) {
			medias.add(cursorToMedia(c));
		}
		c.close();
		close();
		return medias;
	}

	public Media cursorToMedia(Cursor c) {
		if (c.getCount() < 0) {
			return null;
		}

		Media media = new Media(c.getString(MySQLiteDatabase.NUM_COL_TYPE),
				c.getString(MySQLiteDatabase.NUM_COL_NAME),
				c.getInt(MySQLiteDatabase.NUM_COL_VERSION),
				c.getString(MySQLiteDatabase.NUM_COL_PATH),
				c.getInt(MySQLiteDatabase.NUM_COL_ID), Boolean.valueOf(c
						.getString(MySQLiteDatabase.NUM_COL_INSTALLED)));
		media.setInstalling(false);
		media.setNewVersion(c.getInt(MySQLiteDatabase.NUM_COL_NEW_VERSION));
		media.setUpdatable(Boolean.valueOf(c
				.getString(MySQLiteDatabase.NUM_COL_UPDATABLE)));
		return media;
	}
}
