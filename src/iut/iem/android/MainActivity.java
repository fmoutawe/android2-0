package iut.iem.android;

import iut.iem.android.actions.menu.SuggestMenuAction;
import iut.iem.android.fragments.MenuFragment.IActions;
import iut.iem.android.fragments.MenuFragment.IActionsFragment;
import iut.iem.android.fragments.SuggestDialogFragment;
import iut.iem.android.others.Utilities;
import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;

public class MainActivity extends FragmentActivity implements IActionsFragment {

	private SlidingPaneLayout mSlidingPane;
	private ActionBar mActionBar;

	@Override  
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mSlidingPane = (SlidingPaneLayout) findViewById(R.id.sliding_pane);
		if (mSlidingPane != null) {
			
			Animation.AnimationListener listener = new Animation.AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					mSlidingPane.setVisibility(View.VISIBLE);
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					mActionBar = getActionBar();
					mSlidingPane.setPanelSlideListener(new SliderListener());
					panelClosed();
					//mSlidingPane.openPane();
				}
			};
			mSlidingPane.setAnimation(Utilities.inFromRightAnimation(listener));			
		}
	}

	public void closeSlidingPaneIfExist() {
		if (mSlidingPane != null) {
			mSlidingPane.closePane();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home && !mSlidingPane.isOpen()) {
			mSlidingPane.openPane();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onSuggestMenuClick(Uri selectedImage){
		FragmentManager fm = getSupportFragmentManager();
		SuggestDialogFragment suggestDialog = new SuggestDialogFragment();
		suggestDialog.setPath(selectedImage);
		suggestDialog.show(fm, "fragment_edit_name");
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == SuggestMenuAction.RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){
			Uri selectedImage = data.getData();			
			onSuggestMenuClick(selectedImage);
		}
	}
	
	private void panelOpened() {
		mActionBar.setHomeButtonEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setTitle(R.string.menu);

		getSupportFragmentManager().findFragmentById(R.id.left)
				.setHasOptionsMenu(true);
		getSupportFragmentManager().findFragmentById(R.id.right)
				.setHasOptionsMenu(false);
	}
	
	private void panelClosed() {
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setTitle(R.string.app_name);

		getSupportFragmentManager().findFragmentById(R.id.left)
				.setHasOptionsMenu(false);
		getSupportFragmentManager().findFragmentById(R.id.right)
				.setHasOptionsMenu(true);
	}
	
	private class SliderListener extends
			SlidingPaneLayout.SimplePanelSlideListener {
		@Override
		public void onPanelOpened(View panel) {
			panelOpened();
		}

		@Override
		public void onPanelClosed(View panel) {
			// TODO Auto-generated method stub
			panelClosed();
		}	
	}

	@Override
	public void onClick(IActions action, int type) {
		Fragment fragment = getSupportFragmentManager().findFragmentById(
				R.id.right);
		((IActionsFragment) fragment).onClick(action, type);
		if (mSlidingPane != null) {
			mSlidingPane.closePane();
		}
	}

}
