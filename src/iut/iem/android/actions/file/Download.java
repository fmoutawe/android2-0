package iut.iem.android.actions.file;

import iut.iem.android.models.Media;

public interface Download extends FileAction {

    public static interface DownloadCallback {

        void onDownloadStarted(Media media);
        
        void onDownloadProgress(Media media, int progress);

        void onDownloadEnded(Media media, boolean update);

        void onDownloadFailed(Media media);
    }

    public void download(Media media, boolean update, DownloadCallback callback);
    
    public void noMedias(String croutonMessage);

}
