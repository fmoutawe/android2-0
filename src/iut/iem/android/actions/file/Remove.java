package iut.iem.android.actions.file;

import iut.iem.android.models.Media;

public interface Remove extends FileAction {
	
	public static interface RemoveCallback{
		void onRemoveStarted(Media media);
		void onRemoveFailed(Media media);
		void onRemoveEnded(Media media);
	}
	
	void remove(Media media, RemoveCallback callback);
}
