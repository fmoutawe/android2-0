package iut.iem.android.actions.menu;

import iut.iem.android.R;
import iut.iem.android.actions.file.Download;
import iut.iem.android.actions.file.FileAction;
import iut.iem.android.actions.file.Remove;
import iut.iem.android.adapters.MediaAdapter;
import iut.iem.android.fragments.MenuFragment.IActions;
import iut.iem.android.models.Media;
import iut.iem.android.providers.MediaManager;

import java.util.List;

import com.haarman.listviewanimations.swinginadapters.prepared.SwingLeftInAnimationAdapter;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class RemoveMenuAction extends MenuAction implements IActions {

	public static final String LOG_TAG = RemoveMenuAction.class.getSimpleName();

	@Override
	public void proceed(Context context, ListView resultList, TextView title,
			int type, final FileAction action) {
		List<Media> medias = MediaManager.getManager().getInstalledMedias(type);
		MediaAdapter resultAdapter = new MediaAdapter(context, false);
		resultAdapter.updateMedias(medias);
		SwingLeftInAnimationAdapter swingAdapter = new SwingLeftInAnimationAdapter(resultAdapter);
		swingAdapter.setAbsListView(resultList);
		resultList.setAdapter(swingAdapter);
		resultList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> adapter, View v,
							int position, long arg3) {
//							((Remove) action).remove(
//									media,
//									(Remove.RemoveCallback) action);
							
							Media media = (Media) adapter
									.getItemAtPosition(position);
							((Remove)action).remove(media,
									(Remove.RemoveCallback) action);
					}
				});
		String message = " - " + context.getString(R.string.remove);
		setSubTitle(title, message, type);
		if (medias.size() < 1) {
			((Download) action).noMedias(context
					.getString(R.string.nomedia_remove));
		}
	}

}
