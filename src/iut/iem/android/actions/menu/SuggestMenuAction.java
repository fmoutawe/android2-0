package iut.iem.android.actions.menu;

import iut.iem.android.actions.file.FileAction;
import iut.iem.android.fragments.MenuFragment.IActions;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ListView;
import android.widget.TextView;

public class SuggestMenuAction extends MenuAction implements IActions {

	public static final int RESULT_LOAD_IMAGE = 1992;
	
	@Override
	public void proceed(Context context, ListView resultList, TextView title, int type, FileAction download) {
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		((Activity)context).startActivityForResult(i, RESULT_LOAD_IMAGE);
	}


}
