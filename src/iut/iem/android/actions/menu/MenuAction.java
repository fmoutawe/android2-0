package iut.iem.android.actions.menu;

import iut.iem.android.others.Utilities;
import android.widget.TextView;

public class MenuAction {

	public void setSubTitle(TextView title, String message, int type) {
		CharSequence text = (CharSequence) Utilities.idToType(type);
		text = String.valueOf(text.charAt(0)).toUpperCase() + text.subSequence(1, text.length());
		title.setText(text + message);
	}
}
