package iut.iem.android.actions.menu;

import iut.iem.android.R;
import iut.iem.android.actions.file.Download;
import iut.iem.android.actions.file.FileAction;
import iut.iem.android.adapters.MediaAdapter;
import iut.iem.android.fragments.MenuFragment.IActions;
import iut.iem.android.models.Media;
import iut.iem.android.providers.MediaManager;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.haarman.listviewanimations.swinginadapters.prepared.SwingLeftInAnimationAdapter;

public class InstallMenuAction extends MenuAction implements IActions {

	@Override
	public void proceed(final Context context, ListView resultList,
			TextView title, int type, final FileAction download) {
		List<Media> medias = MediaManager.getManager().getNotInstalledMedias(
				type);

		MediaAdapter resultAdapter = new MediaAdapter(context, false);
		resultAdapter.updateMedias(medias);
		SwingLeftInAnimationAdapter swingAdapter = new SwingLeftInAnimationAdapter(resultAdapter);
		swingAdapter.setAbsListView(resultList);
		resultList.setAdapter(swingAdapter);
		resultList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> adapter, View v,
							int position, long arg3) {
						Media media = (Media) adapter
								.getItemAtPosition(position);
						((Download)download).download(media,
								false, (Download.DownloadCallback) download);
					}
				});
		String message = " - " + context.getString(R.string.install);
		setSubTitle(title, message, type);
		if (medias.size() < 1) {
			((Download)download).noMedias(context.getString(R.string.nomedia_install));
		}
	}

}
