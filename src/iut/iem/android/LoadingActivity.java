package iut.iem.android;

import iut.iem.android.components.HoloCircularProgressBar;
import iut.iem.android.models.Media;
import iut.iem.android.others.Utilities;
import iut.iem.android.providers.MediaManager;
import iut.iem.android.services.DownloadMediaFileService;

import java.util.Calendar;
import java.util.List;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.widget.LinearLayout;

public class LoadingActivity extends Activity {
	private static final String LOG_CAT = LoadingActivity.class.getSimpleName();
	private static final int DELAY = 2000;

	private LinearLayout mRootLinearLayout;
	private HoloCircularProgressBar mCircularProgressBar;
	private ObjectAnimator mProgressBarAnimator;
	private int mCount;
	private int mDelay;

	private final Handler mHandler = new Handler();
	private final ManagerReadyToUseReceiver mReceiver = new ManagerReadyToUseReceiver();

	private Runnable mNext = new Runnable() {

		@Override
		public void run() {
			Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
			startActivity(intent);
			finish();
		}
	};
	
	private Runnable mAnimate = new Runnable() {
		public void run(){
			animate(mCircularProgressBar, 1f, mDelay, mCount);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.loading_activity);

		mRootLinearLayout = (LinearLayout) findViewById(R.id.llRoot);
		mCircularProgressBar = (HoloCircularProgressBar) findViewById(R.id.circularProgressBar);
		mCount  = ObjectAnimator.INFINITE;
		mDelay = DELAY;

		MediaManager mediaManager = MediaManager.getManager();
		List<Media> medias = mediaManager.loadMedias(getApplicationContext(),
				"image");
		if (medias.size() < 1) {
			IntentFilter filter = new IntentFilter();
			filter.addAction(Utilities.MEDIA_PARSING_ENDED);
			registerReceiver(mReceiver, filter);
			startDownloadMediaFileService();
			
		} else {
			mCount = 0;
			mDelay = 400;
		}
		
		mHandler.postDelayed(mAnimate, 300);

	}

	public void startDownloadMediaFileService() {
		Calendar calendar = Calendar.getInstance();
		Intent intent = new Intent(this, DownloadMediaFileService.class);
		 PendingIntent pIntent = PendingIntent.getService(this, 0, intent, 0);
		
		 AlarmManager alarm = (AlarmManager)
		 getSystemService(Context.ALARM_SERVICE);
		 alarm.setRepeating(AlarmManager.RTC_WAKEUP,
		 calendar.getTimeInMillis(), 86400000, pIntent);
		startService(intent);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mProgressBarAnimator.cancel();
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mHandler.removeCallbacks(mNext);
		try {
			unregisterReceiver(mReceiver);
		} catch (Exception e) {
		}

	}

	private void animate(final HoloCircularProgressBar progressBar,
			final float progress, final int duration,
			int count) {

		mProgressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress",
				progress);
		mProgressBarAnimator.setDuration(duration);
		mProgressBarAnimator.setRepeatCount(count);
		mProgressBarAnimator.addListener(new AnimatorListener() {

			@Override
			public void onAnimationStart(Animator animation) {
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				Animation.AnimationListener listener = new Animation.AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						mHandler.post(mNext);
					}
				};
				mRootLinearLayout.startAnimation(Utilities
						.outToLeftAnimation(listener));
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}
		});
		mProgressBarAnimator.start();
	}

	public void onReceive(int repeat) {
		mProgressBarAnimator.reverse();
		mProgressBarAnimator.setRepeatCount(repeat);
		mProgressBarAnimator.setDuration(1000);
	}

	public class ManagerReadyToUseReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			LoadingActivity.this.onReceive(0);
		}

	}

}
