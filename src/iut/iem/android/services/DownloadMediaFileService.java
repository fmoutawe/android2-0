package iut.iem.android.services;

import iut.iem.android.others.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DownloadMediaFileService extends IntentService {
	public static final String LOG_TAG = DownloadMediaFileService.class
			.getSimpleName();

	public DownloadMediaFileService() {
		super(LOG_TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		Log.e(LOG_TAG, LOG_TAG);
		
		Context context = getApplicationContext();
		String success = Utilities.MEDIA_DOWNLOAD_ENDED_INTENT_FAILURE;
		String mediaFilePath = null;

		InputStream input = null;
		OutputStream output = null;
		HttpURLConnection connection = null;
		try {
			URL url = new URL(Utilities.MEDIA_URL);
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();

			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				input = connection.getInputStream();
				mediaFilePath = context.getFilesDir().getAbsolutePath()
						+ File.separator + Utilities.MEDIA_FILE_NAME;
				output = new FileOutputStream(mediaFilePath);

				byte data[] = new byte[4096];
				int count;
				while ((count = input.read(data)) != -1) {
					output.write(data, 0, count);
				}
				success = Utilities.MEDIA_DOWNLOAD_ENDED_INTENT_SUCCESS;
			}
		} catch (Exception e) {
			Log.e(LOG_TAG, "Echec de la mise � jour du fichier.");
		} finally {
			try {
				if (output != null)
					output.close();
				if (input != null)
					input.close();
			} catch (IOException ignored) {
			}

			if (connection != null)
				connection.disconnect();
		}

		Intent downloadEnded = new Intent(success);
		sendBroadcast(downloadEnded);
	}

}
