package iut.iem.android.providers;

import iut.iem.android.R;
import iut.iem.android.database.MediaBdd;
import iut.iem.android.models.Media;
import iut.iem.android.others.Utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;
import android.util.SparseArray;

public class MediaManager {
	public static final String LOG_TAG = MediaManager.class.getSimpleName();
	private SparseArrayCompat<List<Media>> mMedias;
	private static MediaManager instance = null;

	public static MediaManager getManager() {
		if (instance == null) {
			instance = new MediaManager();
		}
		return instance;
	}

	public MediaManager() {
		mMedias = new SparseArrayCompat<List<Media>>();
		mMedias.append(Utilities.IMAGE_ID, new ArrayList<Media>());
		mMedias.append(Utilities.TEXT_ID, new ArrayList<Media>());
	}

	public SparseArrayCompat<List<Media>> getMedias() {
		return mMedias;
	}

	public List<Media> getMediasById(int type) {
		return mMedias.get(type);
	}
	
	public SparseArrayCompat<List<Media>> getmMedias() {
		return mMedias;
	}

	public void setmMedias(SparseArrayCompat<List<Media>> mMedias) {
		this.mMedias = mMedias;
	}

	public List<Media> getNotInstalledMedias(int type) {
		List<Media> notInstalledMedias = new ArrayList<Media>();
		List<Media> medias = getMediasById(type);
		for (Media mediaModel : medias) {
			if (!mediaModel.isInstalled()) {
				notInstalledMedias.add(mediaModel);
			}
		}
		return notInstalledMedias;
	}

	public List<Media> getInstalledMedias(int type) {
		List<Media> installedMedias = new ArrayList<Media>();
		List<Media> medias = getMediasById(type);
		for (Media mediaModel : medias) {
			if (mediaModel.isInstalled()) {
				installedMedias.add(mediaModel);
			}
		}
		return installedMedias;
	}
	
	public List<Media> getUpdatableMedias(int type){
		List<Media> updatableMedias = new ArrayList<Media>();
		List<Media> medias = getInstalledMedias(type);
		for(Media media : medias){
			if(media.isUpdatable()){
				updatableMedias.add(media);
			}
		}
		return updatableMedias;
	}

	public boolean addMedia(Media media, Context context) {
		int type = Utilities.typeToId(media.getType());
		if (type >= 0) {
			List<Media> medias = getMediasById(type);
			if (medias.contains(media)) {
				int index = medias.indexOf(media);
				Media currentMedia = medias.get(index);
				if (!currentMedia.equalVersion(media)) {
					if (currentMedia.lowerVersion(media)) {
						currentMedia.setNewVersion(media.getVersion());
						currentMedia.setUpdatable(true);
						save(currentMedia, true, context);
						return true;
					}
				}
			} else {
				medias.add(media);
				save(media, false, context);
			}
		}
		return false;
	}

	public static void isInstalled(Media media) {
		String mediaFilePath = 	Utilities.MEDIA_INTERNAL_PATH + media.getPath();
		File file = new File(mediaFilePath);
		if (file.exists()) {
			media.setInstalled(true);
		}
	}

	public boolean inList(Media media, List<Media> listMedia) {
		return listMedia.contains(media);
	}

	public void installMedia(Media media) {
		List<Media> medias = getMediasById(Utilities.typeToId(media.getType()));
		int position = medias.indexOf(media);
		medias.get(position).setInstalled(true);
	}

	public void save(Media media, boolean update, Context context) {
		MediaBdd.Builder.getInstance(context).open();
		if(update){
			MediaBdd.Builder.getInstance(context).updateMedia(media);
		}else{
			
			MediaBdd.Builder.getInstance(context).insertMedia(media);
		}
		MediaBdd.Builder.getInstance(context).close();
	}

	public List<Media> loadMedias(Context context, String type) {
		List<Media> medias = MediaBdd.Builder.getInstance(context).getMedias();
		for (Media media : medias) {
			isInstalled(media);
			MediaManager.getManager().getmMedias().get(Utilities.typeToId(media.getType())).add(media);
		}
		return medias;

	}
}