package iut.iem.android.tasks;

import iut.iem.android.actions.file.Download.DownloadCallback;
import iut.iem.android.models.Media;
import iut.iem.android.others.Utilities;
import iut.iem.android.providers.MediaManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class DownloadMediaTask extends AsyncTask<Media, Integer, Boolean> {
	public static final String LOG_TAG  = DownloadMediaTask.class.getSimpleName();
	private Context mContext;
	private Media mMedia;
	private DownloadCallback mCallback;
	private String mediaFilePath;
	private boolean mUpdate;
	
	public DownloadMediaTask(Context context, DownloadCallback callback, Media media, boolean update) {
		mContext = context;
		mCallback = callback;
		mMedia = media;
		mUpdate = update;
	}
	
	@Override
	protected Boolean doInBackground(Media... args) {
		InputStream input = null;
		HttpURLConnection connection = null;
		URL url;
		try {
			url = new URL(Utilities.REMOTE_URL + mMedia.getPath());
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();
			
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				int filelength = connection.getContentLength();
				input = connection.getInputStream();
				String mediaFilePath = 	Utilities.MEDIA_INTERNAL_PATH + mMedia.getPath();
				
				File file = new File(mediaFilePath);
				file.getParentFile().mkdirs();
				OutputStream output = new FileOutputStream(file);
				
				byte data[] = new byte[4096];
				long total = 0;
				int count;
				while ((count = input.read(data)) != -1) {
					total += count;
					if (filelength > 0){
						int progress = (int) (total * 100 / (filelength));
						publishProgress(progress);
					}
					output.write(data, 0, count);
				}
				output.close();				
				return true;
			}
			
		}catch (IOException e) {
			Log.e(LOG_TAG, e.getMessage());
		}finally{
			try {
				input.close();
			} catch (IOException e) {
			}
			connection.disconnect();
		}
		
		return false;
	}
	
	@Override
	protected void onPreExecute() {
		mCallback.onDownloadStarted(mMedia);
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		mCallback.onDownloadProgress(mMedia, values[0]);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		if(!result){
			mCallback.onDownloadFailed(mMedia);
		}else{
			mCallback.onDownloadEnded(mMedia, mUpdate);
		}
		
	}

}
