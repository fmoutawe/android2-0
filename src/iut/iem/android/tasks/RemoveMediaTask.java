package iut.iem.android.tasks;

import iut.iem.android.actions.file.Remove.RemoveCallback;
import iut.iem.android.models.Media;
import iut.iem.android.others.Utilities;

import java.io.File;

import android.content.Context;
import android.os.AsyncTask;

public class RemoveMediaTask extends
		AsyncTask<Void, Integer, Boolean> {
	private Context mContext;
	private Media mMedia;
	private RemoveCallback mCallback;
	
	public RemoveMediaTask(Context context, Media media, RemoveCallback callback) {
		mContext = context;
		mMedia = media;
		mCallback = callback;
	}

	@Override
	protected Boolean doInBackground(Void... args) {
		String mediaFilePath = 	Utilities.MEDIA_INTERNAL_PATH + mMedia.getPath();
		File file = new File(mediaFilePath);
		return file.delete();
	}
	
	@Override
	protected void onPreExecute() {
		mCallback.onRemoveStarted(mMedia);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		if(result){
			mCallback.onRemoveEnded(mMedia);
		}else{
			mCallback.onRemoveFailed(mMedia);
		}
	}

}
