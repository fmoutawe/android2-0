package iut.iem.android.tasks;

import iut.iem.android.models.Media;
import iut.iem.android.others.Utilities;
import iut.iem.android.providers.MediaManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;

public class MediaParserTask extends AsyncTask<String, Integer, Boolean> {

	public static final String LOG_TAG = MediaParserTask.class.getSimpleName();

	private Context mContext;
	private boolean mMediaUpdatable = false;

	public MediaParserTask(Context context) {
		this.mContext = context;
	}

	@Override
	protected Boolean doInBackground(String... storage) {
		File file = new File(storage[0]);
		InputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));
			parse(in);
		} catch (Exception e) {
			Log.e(LOG_TAG, e.getMessage());
		}

		return mMediaUpdatable;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if(result){
			Utilities.notificationUpdatableMedia(mContext);
		}
		Intent downloadEnded = new Intent(Utilities.MEDIA_PARSING_ENDED);
		mContext.sendBroadcast(downloadEnded);
	}

	public void parse(InputStream in) throws XmlPullParserException,
			IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			readXmlFeed(parser);
		} finally {
			in.close();
		}
	}

	private void readXmlFeed(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, parser.getNamespace(), "medias");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("media")) {
				readXmlMedia(parser);
			} else {
				skip(parser);
			}
		}
	}

	private void readXmlMedia(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, parser.getNamespace(), "media");
		String type = parser.getAttributeValue(parser.getNamespace(), "type");
		if (type != null) {
			String name = (parser.getAttributeValue(parser.getNamespace(),
					"name") != null) ? parser.getAttributeValue(
					parser.getNamespace(), "name") : null;
			Integer versionCode = (parser.getAttributeValue(
					parser.getNamespace(), "versionCode") != null) ? Integer
					.parseInt(parser.getAttributeValue(parser.getNamespace(),
							"versionCode")) : 0;
			String path = (parser.getAttributeValue(parser.getNamespace(),
					"path") != null) ? parser.getAttributeValue(
					parser.getNamespace(), "path") : null;
			Media media = new Media(type, name, versionCode, path);
			MediaManager.isInstalled(media);

			if (MediaManager.getManager().addMedia(media, mContext.getApplicationContext())) {
				mMediaUpdatable = true;
			}
		}
		parser.next();
	}

	private void skip(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

}
