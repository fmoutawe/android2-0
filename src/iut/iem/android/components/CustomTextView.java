package iut.iem.android.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView {

	public CustomTextView(Context context) {
		super(context);
		makeTheTypeFace(context);
	}
	
	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		makeTheTypeFace(context);
	}

	public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		makeTheTypeFace(context);
	}

	private void makeTheTypeFace(Context context){
		if (isInEditMode())
			return;
		
		Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/frutigernextlt_bold.ttf");
		this.setTypeface(face);
		
	}
	
}
