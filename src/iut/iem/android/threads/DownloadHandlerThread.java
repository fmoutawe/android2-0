package iut.iem.android.threads;

import iut.iem.android.actions.file.Download;
import iut.iem.android.models.Media;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

/**
 * Simple HandlerThread permettant de tŽlŽcharger des images en t‰che de fond.
 * 
 * @author Cyril Mottier
 */
public class DownloadHandlerThread /*extends HandlerThread implements Download*/ {

    private static final String LOG_TAG = DownloadHandlerThread.class.getSimpleName();
//
//    /**
//     * Identifiants "what" utilisŽs par le Handler s'occupant de faire les
//     * appels aux callbacks dans le thread principal
//     */
//    private static final int DOWNLOAD_STARTED = 1;
//    private static final int DOWNLOAD_FAILED = 2;
//    private static final int DOWNLOAD_ENDED = 4;
//
//    /**
//     * Identifiant "what" utilisŽ par le Handler associŽ au Looper en t‰che de
//     * fond.
//     */
//    private static final int MESSAGE_DOWNLOAD = 1;
//
//    /**
//     * Wrapper permettant de faire passer de l'information ˆ travers les
//     * Messages.
//     * 
//     * @author Cyril Mottier
//     */
//    private static class DowloadInfo {
//        public String url;
//        public WeakReference<DownloadCallback> callback;
//        public Object obj;
//
//        public DowloadInfo(String url, DownloadCallback callback) {
//            this.url = url;
//            this.callback = callback == null ? null : new WeakReference<DownloadCallback>(callback);
//        }
//    }
//
//    /**
//     * Handler permettant de communiquer avec le "main thread"
//     */
//    private Handler mMainHandler;
//
//    /**
//     * Handler associŽ au {@link HandlerThread} courant
//     */
//    private Handler mDownloadHandler;
//
//    public DownloadHandlerThread() {
//        super(LOG_TAG, Process.THREAD_PRIORITY_BACKGROUND);
//        mMainHandler = new Handler(Looper.getMainLooper(), mMainCallback);
//    }
//
//    @Override
//    protected void onLooperPrepared() {
//        super.onLooperPrepared();
//        synchronized (this) {
//            mDownloadHandler = new Handler(getLooper(), mDownloadCallback);
//            notifyAll();
//        }
//    }
//
//    @Override
//    public boolean quit() {
//        if (mDownloadHandler != null) {
//            mDownloadHandler.removeMessages(MESSAGE_DOWNLOAD);
//            mDownloadHandler = null;
//        }
//        return super.quit();
//    };
//
//    @Override
//    public void download(Media media, DownloadCallback callback) {
//        
//        if (!isAlive()) {
//            return;
//        }
//
//        synchronized (this) {
//            while (isAlive() && mDownloadHandler == null) {
//                try {
//                    wait();
//                } catch (InterruptedException e) {
//                }
//            }
//        }
//
//        if (mDownloadHandler != null) {
//            Message message = mDownloadHandler.obtainMessage();
//            message.what = MESSAGE_DOWNLOAD;
//            message.obj = new DowloadInfo(media, callback);
//
//            mDownloadHandler.sendMessage(message);
//        }
//    }
//
//    private Callback mDownloadCallback = new Callback() {
//
//        public boolean handleMessage(Message msg) {
//            switch (msg.what) {
//                case MESSAGE_DOWNLOAD:
//                    
//                    final Handler h = mMainHandler;
//
//                    final DowloadInfo downloadInfo = (DowloadInfo) msg.obj;
//                    if (downloadInfo == null) {
//                        return false;
//                    }
//
//                    InputStream input = null;
//        			OutputStream output = null;
//        			HttpURLConnection connection = null;
//                    
//                    try {
//
//                        Message.obtain(h, DOWNLOAD_STARTED).sendToTarget();
//
//                        URL url = new URL(downloadInfo.url);
//                        
//        				connection = (HttpURLConnection) url.openConnection();
//        				connection.connect();
//
////        				if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
////        					input = connection.getInputStream();
////        					output = new FileOutputStream(Environment.get.getExternalCacheDir() + "/medias.xml");
////        										
////        					byte data[] = new byte[4096];
////        					int count;
////        					while ((count = input.read(data)) != -1) {
////        						output.write(data, 0, count);
////        					}
////        				} 
//
////                        downloadInfo.obj = bitmap;
//                        Message.obtain(h, DOWNLOAD_ENDED, downloadInfo).sendToTarget();
//
//                    } catch (Exception e) {
//                        downloadInfo.obj = e;
//                        Message.obtain(h, DOWNLOAD_FAILED, downloadInfo).sendToTarget();
//                    }finally {
//        				try {
//        					if (output != null)
//        						output.close();
//        					if (input != null)
//        						input.close();
//        				} catch (IOException ignored) {}
//
//        				if(connection != null)
//        					connection.disconnect();
//        			}
//            }
//
//            return true;
//        }
//    };
//
//    private Callback mMainCallback = new Callback() {
//
//        @Override
//        public boolean handleMessage(Message msg) {
//
//            if (mDownloadHandler == null) {
//                return false;
//            }
//
//            final DowloadInfo downloadInfo = (DowloadInfo) msg.obj;
//            if (downloadInfo == null) {
//                return false;
//            }
//
//            final DownloadCallback callback = (downloadInfo.callback == null) ? null : downloadInfo.callback.get();
//            if (callback == null) {
//                return false;
//            }
//
//            switch (msg.what) {
//                case DOWNLOAD_STARTED:
//                    callback.onDownloadStarted(DownloadHandlerThread.this);
//                    break;
//
//                case DOWNLOAD_FAILED:
//                    callback.onDownloadFailed(DownloadHandlerThread.this);
//                    break;
//
//                case DOWNLOAD_ENDED:
//                    callback.onDownloadEnded(DownloadHandlerThread.this);
//                    break;
//
//                default:
//                    return false;
//            }
//
//            return true;
//        }
//    };

}
