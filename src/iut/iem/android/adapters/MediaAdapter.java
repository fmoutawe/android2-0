package iut.iem.android.adapters;

import iut.iem.android.R;
import iut.iem.android.holder.ViewHolder;
import iut.iem.android.models.Media;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MediaAdapter extends BaseAdapter {

	private List<Media> mMedias = Collections.emptyList();
	
	private Context mContext;
	private boolean mUpdate;
	
	public MediaAdapter(Context context, boolean update) {
		mContext = context;
		mUpdate = update;
	}
	
	public void updateMedias(List<Media> medias){
		mMedias = medias;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mMedias.size();
	}

	@Override
	public Media getItem(int position) {
		return mMedias.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			convertView = LayoutInflater.from(mContext).inflate(R.layout.result_item, parent, false);
		}
		
		TextView resultTitle = ViewHolder.get(convertView, R.id.title);
		TextView resultVersion = ViewHolder.get(convertView, R.id.version);
		TextView resultVersionUpdate = ViewHolder.get(convertView, R.id.versionUpdate);
		TextView resultType = ViewHolder.get(convertView, R.id.type);
		ProgressBar resultProgress = ViewHolder.get(convertView, R.id.progress);
		
		Media media = getItem(position);
		media.setProgressBar(resultProgress);
		resultTitle.setText(media.getName());
		resultVersion.setText(String.valueOf(media.getVersion()));
		resultVersionUpdate.setText(String.valueOf(media.getNewVersion()));
		if(mUpdate){
			resultVersionUpdate.setVisibility(View.VISIBLE);
		}
		resultType.setText(media.getType());
		if(media.isInstalling()){
			resultProgress.setVisibility(View.VISIBLE);
			resultProgress.setProgress(media.getProgress());
			
		}else{
			resultProgress.setVisibility(View.INVISIBLE);
			resultProgress.setProgress(0);
			media.setProgress(0);
		}
		
		return convertView;
	}
	
	public void remove(Media media){
		mMedias.remove(media);
	}

}
