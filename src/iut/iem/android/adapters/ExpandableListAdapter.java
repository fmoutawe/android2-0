package iut.iem.android.adapters;

import iut.iem.android.R;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.support.v4.util.SparseArrayCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	//Group label
	private List<String> mListDataHeader;//Data child
	private SparseArrayCompat<List<String>> mListDataChild;
	private IExpandableListAdapter mIExpandableListAdapter;
	private int lastExpandedGroupPosition;
	private static final int[] mIcons = {R.drawable.ic_action_picture, R.drawable.ic_action_edit};
		
	public ExpandableListAdapter(Context context, List<String> listDataHeader, SparseArrayCompat<List<String>> listDataChild, IExpandableListAdapter iExpandableListAdapter) {
		mContext = context;
		mListDataHeader = listDataHeader;
		mListDataChild = listDataChild;
		mIExpandableListAdapter = iExpandableListAdapter;
	}
	
	public ExpandableListAdapter(Context context, List<String> listDataHeader, SparseArrayCompat<List<String>> listDataChild) {
		this(context, listDataHeader, listDataChild, null);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mListDataChild.get(groupPosition).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final String childText = (String) getChild(groupPosition, childPosition);
		
		if(convertView == null){
			LayoutInflater infalInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.menu_list_item, null);
		}
		
		TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
		txtListChild.setText(childText);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mListDataChild.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mListDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return mListDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		
		if(convertView == null){
			LayoutInflater infalInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.menu_list_group, null);
		}
		
		TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
		lblListHeader.setText(headerTitle);
		lblListHeader.setCompoundDrawablesWithIntrinsicBounds(mIcons[groupPosition], 0, 0, 0);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	@Override
	public void onGroupExpanded(int groupPosition) {
		if(groupPosition != lastExpandedGroupPosition){
			if(mIExpandableListAdapter != null){
				mIExpandableListAdapter.onGroupExpanded(lastExpandedGroupPosition);
			}
		}
		super.onGroupExpanded(groupPosition);
		lastExpandedGroupPosition = groupPosition;
	}
	
	public interface IExpandableListAdapter{
		void onGroupExpanded(int lastExpandedGroupPosition);
	}

}
