package iut.iem.android.others;

import iut.iem.android.MainActivity;
import iut.iem.android.R;

import java.io.File;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

public final class Utilities {

//	public static final String MEDIA_URL = "http://lionel.banand.free.fr/lp_iem/updaterLPIEM.php?serial=AAA&type=medias";
	public static final String MEDIA_URL = "http://blogdechloe.com/package.xml";
	public static final String REMOTE_URL = "http://lionel.banand.free.fr/lp_iem/";
	public static final String MEDIA_INTERNAL_PATH = Environment.getExternalStoragePublicDirectory("medias") + File.separator;
	public static final String MEDIA_FILE_NAME = "medias.xml";
	public static final String MEDIA_DOWNLOAD_ENDED_INTENT_FAILURE = "iut.iem.android.intent.action.MEDIA_DOWNLOAD_ENDED_FAILURE";
	public static final String MEDIA_DOWNLOAD_ENDED_INTENT_SUCCESS = "iut.iem.android.intent.action.MEDIA_DOWNLOAD_ENDED_SUCCESS";
	public static final String MEDIA_PARSING_ENDED = "iut.iem.android.intent.action.MEDIA_PARSING_ENDED";
	public static final String MEDIA_DOWNLOAD_ENDED_STATUS = "MediaFile.state";
	public static final String MEDIA_DOWNLOAD_ENDED_FILE_PATH = "MediaFile.path";
	public static final String SUGGEST_TYPE_EXTRA = "Suggest.type";
	public static final int NOTIFICATION_ID = 1;

	public static final int IMAGE_ID = 0;
	public static final int TEXT_ID = 1;

	public static final String IMAGE = "image";
	public static final String TEXT = "texte";

	public static void notificationFailureUpdateMedia(Context context) {
		sendInformationNotification(context,
				context.getString(R.string.failure_media_file_download));
	}

	public static void notificationUpdatableMedia(Context context) {
		sendInformationNotification(context,
				context.getString(R.string.updatable_media));
	}

	public static void sendInformationNotification(Context context,
			String message) {
		sendInformationNotification(context, message, NOTIFICATION_ID);
	}

	public static void sendInformationNotification(Context context,
			String message, int notificationID) {
		sendNotification(context, context.getString(R.string.app_name),
				message, notificationID);
	}

	public static void sendNotificationWithCustomTitle(Context context,
			String title, String message) {
		sendNotificationWithCustomTitle(context, title, message,
				NOTIFICATION_ID);
	}

	public static void sendNotificationWithCustomTitle(Context context,
			String title, String message, int notificationID) {
		sendNotification(context, title, message, notificationID);
	}

	public static void sendNotification(Context context, String title,
			String message, int notificationID) {
		NotificationManager nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder notification = new NotificationCompat.Builder(
				context);
		notification.setSmallIcon(R.drawable.launcher);
		notification.setContentTitle(title);
		notification.setContentText(message);
		
		Intent showIntent = new Intent(context, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, showIntent, 0);
		notification.setContentIntent(contentIntent);
		
		nm.notify(notificationID, notification.build());
	}

	public static int typeToId(String type) {
		if (type.equals(IMAGE)) {
			return IMAGE_ID;
		} else {
			if (type.equals(TEXT)) {
				return TEXT_ID;
			}
		}

		return -1;
	}

	public static String idToType(int type) {
		switch (type) {
		case IMAGE_ID:
			return IMAGE;
		case TEXT_ID:
			return TEXT;
		default:
			return null;
		}
	}

	public static Animation inFromRightAnimation(
			Animation.AnimationListener listener) {

		Animation inFromRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromRight.setDuration(200);
		inFromRight.setFillAfter(true);
		inFromRight.setInterpolator(new AccelerateInterpolator());
		inFromRight.setAnimationListener(listener);
		return inFromRight;
	}

	public static Animation outToLeftAnimation(
			Animation.AnimationListener listener) {
		Animation outtoLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		outtoLeft.setDuration(200);
		outtoLeft.setFillAfter(true);
		outtoLeft.setInterpolator(new AccelerateInterpolator());
		outtoLeft.setAnimationListener(listener);
		return outtoLeft;
	}

	public static Animation inFromLeftAnimation(
			Animation.AnimationListener listener) {
		Animation inFromLeft = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, -1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		inFromLeft.setDuration(500);
		inFromLeft.setFillAfter(true);
		inFromLeft.setInterpolator(new AccelerateInterpolator());
		inFromLeft.setAnimationListener(listener);
		return inFromLeft;
	}

	public static Animation outToRightAnimation(
			Animation.AnimationListener listener) {
		Animation outtoRight = new TranslateAnimation(
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, +1.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f,
				Animation.RELATIVE_TO_PARENT, 0.0f);
		outtoRight.setDuration(500);
		outtoRight.setFillAfter(true);
		outtoRight.setInterpolator(new AccelerateInterpolator());
		outtoRight.setAnimationListener(listener);
		return outtoRight;
	}
}
