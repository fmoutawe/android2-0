package iut.iem.android.fragments;

import iut.iem.android.R;
import iut.iem.android.services.DownloadMediaFileService;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Load the preferences from an XML resource
		addPreferencesFromResource(R.layout.settings);
		Preference button = (Preference) findPreference("button");
		button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				getActivity().startService(new Intent(getActivity(), DownloadMediaFileService.class));
				return true;
			}
		});
	}
}
