package iut.iem.android.fragments;

import iut.iem.android.R;
import iut.iem.android.SettingsActivity;
import iut.iem.android.actions.file.Download;
import iut.iem.android.actions.file.Remove;
import iut.iem.android.actions.menu.InstallMenuAction;
import iut.iem.android.adapters.MediaAdapter;
import iut.iem.android.fragments.MenuFragment.IActions;
import iut.iem.android.fragments.MenuFragment.IActionsFragment;
import iut.iem.android.models.Media;
import iut.iem.android.others.Utilities;
import iut.iem.android.tasks.DownloadMediaTask;
import iut.iem.android.tasks.RemoveMediaTask;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.haarman.listviewanimations.swinginadapters.prepared.SwingLeftInAnimationAdapter;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ContentFragment extends Fragment implements IActionsFragment,
		Download, Download.DownloadCallback, Remove, Remove.RemoveCallback {
	public static final String LOG_TAG = ContentFragment.class.getSimpleName();
	private ListView mResultsView;
	private TextView mTitleTextView;
	private IActions mAction;
	private int mType = Utilities.IMAGE_ID;
	private Crouton mCrouton;
	private LinearLayout mContent;

	public void setmAction(IActions action) {
		this.mAction = action;
	}

	public void setmType(int mType) {
		this.mType = mType;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.content_fragment, container,
				false);
		mTitleTextView = (TextView) view.findViewById(R.id.content_title);
		mResultsView = (ListView) view.findViewById(R.id.content_list);
		mContent = (LinearLayout) view.findViewById(R.id.content);
		onClick(new InstallMenuAction(), Utilities.IMAGE_ID);
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.results_fragment_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			if (mAction != null) {
				onClick(mAction, mType);
			}
			break;
		case R.id.action_settings:
			startActivity(new Intent(getActivity(), SettingsActivity.class));
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(IActions action, int type) {
		action.proceed(getActivity(), mResultsView, mTitleTextView, type, this);
		setmAction(action);
		setmType(type);
		if (mCrouton != null) {
			mCrouton.hide();
		}
	}

	@Override
	public void download(Media media, boolean update, DownloadCallback callback) {
		if ((!media.isInstalled() && !media.isInstalling()) || media.isUpdatable()) {
			DownloadMediaTask downloadMediaTask = new DownloadMediaTask(
					getActivity(), callback, media, update);
			downloadMediaTask.execute();
		}
	}

	@Override
	public void noMedias(String croutonMessage) {
		if (mCrouton != null) {
			mCrouton.cancel();
		}
		mCrouton = Crouton.makeText((Activity) getActivity(), croutonMessage,
				Style.ALERT, mContent).setConfiguration(
				new Configuration.Builder().setDuration(
						Configuration.DURATION_INFINITE).build());
		mCrouton.show();

	}

	@Override
	public void onDownloadStarted(Media media) {
		media.getProgressBar().setVisibility(View.VISIBLE);
		media.setInstalling(true);
	}

	@Override
	public void onDownloadProgress(Media media, int progress) {
		media.getProgressBar().setVisibility(View.VISIBLE);
		media.setProgress(progress);
		media.getProgressBar().setProgress(progress);
	}

	@Override
	public void onDownloadEnded(Media media, boolean update) {
		media.setInstalled(true);
		media.setInstalling(false);
		if(update){
			media.setUpdatable(false);
			media.setVersion(media.getNewVersion());
		}
		
		SwingLeftInAnimationAdapter swingAdapter = (SwingLeftInAnimationAdapter) mResultsView
				.getAdapter();
		BaseAdapter base = swingAdapter.getDecoratedBaseAdapter();
		MediaAdapter mediaAdapter = (MediaAdapter) base;
		if (mediaAdapter.getCount() == 1) {
			noMedias(getString(R.string.nomedia_install));
		}
		mediaAdapter.remove(media);
		
		swingAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDownloadFailed(Media media) {

	}

	@Override
	public void onRemoveStarted(Media media) {

	}

	@Override
	public void onRemoveFailed(Media media) {

	}

	@Override
	public void onRemoveEnded(Media media) {
		media.setInstalled(false);
		SwingLeftInAnimationAdapter swingAdapter = (SwingLeftInAnimationAdapter) mResultsView
				.getAdapter();
		BaseAdapter base = swingAdapter.getDecoratedBaseAdapter();
		MediaAdapter mediaAdapter = (MediaAdapter) base;
		mediaAdapter.remove(media);
		if (mediaAdapter.getCount() < 1) {
			noMedias(getString(R.string.nomedia_remove));
		}
		swingAdapter.notifyDataSetChanged();

	}

	@Override
	public void remove(Media media, RemoveCallback callback) {
		RemoveMediaTask removeMediaTask = new RemoveMediaTask(getActivity(),
				media, callback);
		removeMediaTask.execute();
	}

}
