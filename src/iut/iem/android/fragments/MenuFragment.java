package iut.iem.android.fragments;

import iut.iem.android.R;
import iut.iem.android.actions.file.FileAction;
import iut.iem.android.actions.menu.InstallMenuAction;
import iut.iem.android.actions.menu.RemoveMenuAction;
import iut.iem.android.actions.menu.SuggestMenuAction;
import iut.iem.android.actions.menu.UpdateMenuAction;
import iut.iem.android.adapters.ExpandableListAdapter;
import iut.iem.android.adapters.ExpandableListAdapter.IExpandableListAdapter;
import iut.iem.android.others.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.SparseArrayCompat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuFragment extends Fragment implements IExpandableListAdapter {

	private ExpandableListAdapter mListAdapter;
	private ExpandableListView mExpListView;
	private List<String> mListDataHeader;
	
	private SparseArrayCompat<List<String>> mListDataChild;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.menu_fragment, container,
				false);

		// get the listview
		mExpListView = (ExpandableListView) view
				.findViewById(R.id.actions_list);

		// prepareListData
		prepareListData();

		mListAdapter = new ExpandableListAdapter(getActivity(),
				mListDataHeader, mListDataChild, this);
		

		// setting list adapter
		mExpListView.setAdapter(mListAdapter);
		//Open first group
		mExpListView.expandGroup(0);
		// Add short push listener on child
		mExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Activity parentActivity = getActivity();
                if(parentActivity != null && parentActivity instanceof IActionsFragment){
                	//Create an IActionsFragment object with parentActivity of active fragment
                	IActionsFragment iActionsFragment = (IActionsFragment) parentActivity;
                	
                	//Execute the right method about child position 
                	doTheRightMethodAccordingToTheChildPosition(childPosition, iActionsFragment, groupPosition);
                }
                return false;
            }
        });

		return view;
	}

	/**
	 * Prepare data to expandable list
	 */
	private void prepareListData() {
		mListDataHeader = new ArrayList<String>();
		mListDataChild = new SparseArrayCompat<List<String>>();
		
		// Adding header data
		mListDataHeader.add(Utilities.IMAGE_ID, getString(R.string.image));
		mListDataHeader.add(Utilities.TEXT_ID, getString(R.string.text));

		// Adding child data
		ArrayList<String> actions = new ArrayList<String>();
		
		actions.add(getString(R.string.install));
		actions.add(getString(R.string.update));
		actions.add(getString(R.string.remove));
		
		mListDataChild.append(Utilities.TEXT_ID, (ArrayList<String>) actions.clone());
		
		actions.add(getString(R.string.suggest));
		mListDataChild.append(Utilities.IMAGE_ID, (ArrayList<String>) actions.clone());
	}
	
	/**
	 * 
	 */
	private void doTheRightMethodAccordingToTheChildPosition(int childPosition, IActionsFragment iActionsFragment, int type){
		IActions action = null;
		switch(childPosition){
		case 0:
			 action = new InstallMenuAction();
			break;
		case 1:
			action = new UpdateMenuAction();
			break;
		case 2:
			action = new RemoveMenuAction();
			break;
		case 3:
			action = new SuggestMenuAction();
			break;
		}
		iActionsFragment.onClick(action, type);
	}

	/**
	 * Provide by ExpandableListAdapter
	 */
	@Override
	public void onGroupExpanded(int lastExpandedGroupPosition) {
		mExpListView.collapseGroup(lastExpandedGroupPosition);
	}

	public interface IActionsFragment {
		void onClick(IActions action, int type);
	}
	
	public interface IActions {
		void proceed(Context context, ListView resultList, TextView title, int type, FileAction download);
	}
}
