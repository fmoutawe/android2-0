package iut.iem.android.fragments;

import iut.iem.android.R;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class SuggestDialogFragment extends DialogFragment {

	public Uri mSelectedImage;
	
	public SuggestDialogFragment() {
	}
	
	public void setPath(Uri selectedImage){
		mSelectedImage = selectedImage;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.suggest_dialog_fragment, container);
		getDialog().setTitle(getString(R.string.suggest_image));
		ImageView suggestImage = (ImageView) view.findViewById(R.id.suggest);
		if(mSelectedImage != null){
			suggestImage.setImageURI(mSelectedImage);
			Button suggestButton = (Button) view.findViewById(R.id.suggestButton);
			suggestButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Toast.makeText(getActivity(), getString(R.string.suggest_success), Toast.LENGTH_LONG).show();
					SuggestDialogFragment.this.dismiss();
				}
			});
		}
		return view;
	}

}
