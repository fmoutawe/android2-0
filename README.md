# IUT LP IEM
Media Application

==============================

Credits : 
 - Pierrick Marmot
 - Flavien Moutawe
 
==============================

Ressources : 

#Ecran d'acceuil
https://github.com/passsy/android-HoloCircularProgressBar

#CustomTextView
http://stackoverflow.com/questions/9477336/how-to-make-custom-textview

#Base de donn�es
http://www.tutomobile.fr/comment-utiliser-sqlite-sous-android-tutoriel-android-n%C2%B019/19/10/2010/

#DownloadManager & BroadCastReceiver
http://blog.vogella.com/2011/06/14/android-downloadmanager-example/
http://stackoverflow.com/questions/9799613/how-to-run-the-service-from-8-am-to-8-pm-everyday-in-android

#Download file by Service
http://stackoverflow.com/questions/3028306/download-a-file-with-android-and-showing-the-progress-in-a-progressdialog

#Notifications
http://developer.android.com/guide/topics/ui/notifiers/notifications.html
http://developer.android.com/training/notify-user/display-progress.html

#Programmation Concurrent
http://techtej.blogspot.com.es/2011/03/android-thread-constructspart-4.html
http://stackoverflow.com/questions/15524280/service-vs-intent-service

#Intent Service
http://www.vogella.com/tutorials/AndroidServices/article.html#tutorial_intentservice
http://stackoverflow.com/questions/7845660/how-to-run-a-service-every-day-at-noon-and-on-every-boot #With Calendar
http://stackoverflow.com/questions/4562757/alarmmanager-android-every-day #Width Calendar


