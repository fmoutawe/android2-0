# IUT LP IEM
Media Application

==============================

Credits : 
 - Pierrick Marmot
 - Flavien Moutawe
 
==============================

#ExpandableListView
http://www.androidhive.info/2013/07/android-expandable-list-view-tutorial/
http://www.piwai.info/android-adapter-good-practices/#Custom-item-ViewGroup

#DialogFragment
http://developer.android.com/reference/android/app/DialogFragment.html
http://android-developers.blogspot.fr/2012/05/using-dialogfragments.html

#Crouton
https://github.com/keyboardsurfer/Crouton
http://www.throrinstudio.com/dev/android/toast-et-crouton-sortez-vos-grilles-pains/
http://cyrilmottier.com/2012/07/24/the-making-of-prixing-4-activity-tied-notifications/

#Swipe
https://github.com/nhaarman/ListViewAnimations